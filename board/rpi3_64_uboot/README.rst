Just after burning the image
============================

Prompt
------

**NOTICE: in order to ensure USB keyboard will be found, press <ENTER> a few
times while the RPI3 is powering on.**

#. check the prompt appears in the HDMI console::

	Net:  No ethernet found.
	starting USB...
	Bus usb@7e980000: USB DWC2
	scanning bus usb@7e980000 for devices... 4 USB Devices(s) found
	       scanning usb for storage devices... 0 Storage Devices(s) found
	U-Boot>


Troubleshooting commands
------------------------

#. check the U-Boot and toolchain versions appear::

	U-Boot> version
	U-Boot 2022.04 (Aug 12 2023 - 20:44:46 +0000)

	aarch64-linux-gcc.br_real (Buildroot 2021.11-aarch64428-g6b6741b) 12.2.0
	GNU ld (GNU Binutils) 2.39
	U-Boot>

#. check a long list of commands appear, including ``version``::

	U-Boot> help
	...
	usb       - USB sub-system
	usbboot   - boot from USB device
	version   - print monitor, compiler and linker version
	U-Boot>

#. check info about the board appears, including ``ethaddr``::

	U-Boot> bdinfo
	...
	Build       = 64-bit
	current eth = smsc95xx_eth
	ethaddr     = b8:27:eb:00:00:00
	...
	Early malloc usage: 738 / 2000
	U-Boot>

Access to external devices
--------------------------

#. check USB keyboard is displayed::

	U-Boot> usb tree
	USB device tree:
	  1 Hub (480 Mb/s, 0mA)
	  |  U-Boot Root Hub
	  |
	  +-2  Hub (480 Mb/s, 2mA)
	    |
	    +-3  Vendor specific (480 Mb/s, 2mA)
	    |
	    +-4  Human Interface (12 Mb/s, 100mA)
	          2.4G Mouse

	U-Boot>


#. check partition ``BOOT`` is displayed::

	U-Boot> mmc part

	Partition Map for MMC device 0 --  Partition Type: DOS

	Part    Start Sector    Num Sectors   UUID            Type
	  1     1               65536         00000000-01     0c Boot
	U-Boot>

#. check contents of partition ``BOOT`` are shown:
   ``bootcode.bin``,
   ``config.txt``,
   ``fixup.dat``,
   ``Image`` (U-Boot image renamed to work with RPI3),
   ``start.elf``::

	U-Boot> fatls mmc 0:1
	   429184   Image
	    52460   bootcode.bin
	       67   config.txt
	     7223   fixup.dat
	  2964864   start.elf

	5 files(s), 0 dir(s)

	U-Boot>

Reboot and persistence
----------------------

#. check command reset works (and press a few <ENTER>)::

	U-Boot> reset

	...

	Net:  No ethernet found.
	starting USB...
	Bus usb@7e980000: USB DWC2
	scanning bus usb@7e980000 for devices... 4 USB Devices(s) found
	       scanning usb for storage devices... 0 Storage Devices(s) found
	U-Boot>

#. check environment is persisted::

	U-Boot> printenv testvar
	## Error: "testvar" not defined
	U-Boot> setenv testvar 1
	U-Boot> printenv testvar
	testvar=1
	U-Boot> env save
	Saving Environment to FAT... OK
	U-Boot> reset


	U-Boot> printenv testvar
	testvar=1

#. power off and power on RPI3 and ensure the variable still exists::

	U-Boot> printenv testvar
	testvar=1


#. check partition ``BOOT`` now includes also a file named ``uboot.env``::

	U-Boot> fatls mmc 0:1
	   429184   Image
	    52460   bootcode.bin
	       67   config.txt
	     7223   fixup.dat
	  2964864   start.elf
	    16384   uboot.env

	6 files(s), 0 dir(s)

	U-Boot>

Network
-------

#. ping::

	U-Boot> setenv ipaddr 192.168.0.204
	U-Boot> ping 192.168.0.18
	Waiting for Ethernet connection... done.
	Using smsc95xx_eth device
	host 192.168.0.18 is alive
	U-Boot>
