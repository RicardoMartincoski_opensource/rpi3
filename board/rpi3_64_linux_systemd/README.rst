qemu
====

#. start que qemu after building the image::

	PC $ board/rpi3_64_linux_systemd/qemu.sh
	+ BOARD=rpi3_64_linux_systemd
	+ [ ! -f output/rpi3_64_linux_systemd/images/sdcard.img ]
	...
	Image resized.
	+ utils/docker-run qemu-system-aarch64 -M raspi3b -nographic -kernel output/rpi3_64_linux_systemd/images/Image -append root=/dev/mmcblk0p2 rootwait console=ttyAMA0 -dtb output/rpi3_64_linux_systemd/images/bcm2710-rpi-3-b-plus.dtb -drive file=output/rpi3_64_linux_systemd/images/sdcard.img.qemu,format=raw
	[    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
	[    0.000000] Linux version 5.15.87-v8 (br-user@46a14fb59054) (aarch64-linux-gcc.br_real (Buildroot 2021.11-4428-g6b6741b) 12.2.0, GNU ld (GNU Binutils) 2.39) #1 SMP PREEMPT Tue Aug 22 02:00:36 UTC 2023
	[    0.000000] Machine model: Raspberry Pi 3 Model B+
	...

#. check the login prompt appears in the terminal::

	Welcome to RPI3 using Buildroot
	rpi3 login:

#. login with user root / password root::

	rpi3 login: root
	Password:
	#

#. check the version of the repository used to generate the image::

	# cat /etc/version
	rpi3 @ v0.11-7-g7a400e6
	#

#. poweroff qemu image::

	# poweroff
	...
	[  110.210639] reboot: Power down
	PC $

RPI3, just after burning the image
==================================

#. burn the image::

	PC $ mount | grep sdb
	/dev/sdb1 on /media/ricardo/BOOT type vfat (rw,nosuid,nodev,relatime,uid=1000,gid=1000,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,showexec,utf8,flush,errors=remount-ro,uhelper=udisks2)
	/dev/sdb2 on /media/ricardo/rootfs type ext4 (rw,nosuid,nodev,relatime,errors=remount-ro,uhelper=udisks2)
	/dev/sdb3 on /media/ricardo/rootfs1 type ext4 (rw,nosuid,nodev,relatime,errors=remount-ro,uhelper=udisks2)
	/dev/sdb4 on /media/ricardo/persist type ext4 (rw,nosuid,nodev,relatime,errors=remount-ro,uhelper=udisks2)
	PC $ sudo umount /dev/sdb*
	umount: /dev/sdb: not mounted.
	PC $ mount | grep sdb
	PC $ time sudo dd if=output/rpi3_64_linux_systemd/images/sdcard.img of=/dev/sdb bs=2G count=1
	0+1 records in
	0+1 records out
	520094208 bytes (520 MB, 496 MiB) copied, 169,522 s, 3,1 MB/s
	real    2m49,585s
	user    0m0,005s
	sys     0m0,005s
	PC $

Console login
-------------

#. check the login prompt appears in the HDMI console::

	Welcome to RPI3 using Buildroot
	rpi3 login:

#. connect a USB keyboard

#. login with user root / password root::

	rpi3 login: root
	Password:
	#

#. check IP got from DHCP::

	# ip addr | grep -w inet
	    inet 127.0.0.1/8 scope host lo
	    inet 182.168.0.205/24 brd 182.168.0.255 scope global dynamic eth0
	#

SSH connection
--------------

#. at very first ssh connection the RPI3 host key must be accept by the PC::

	PC $ ssh root@192.168.0.205
	The authenticity of host '192.168.0.205 (192.168.0.205)' can't be established.
	ED25519 key fingerprint is SHA256:cutmbvhYAyphdC5iI7iyz91/Bm2nzdqC+KX+xpbTSKI.
	This key is not known by any other names
	Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
	Warning: Permanently added '192.168.0.205' (ED25519) to the list of known hosts.
	root@192.168.0.205's password:
	#

#. next ssh connection won't ask to accept host key::

	PC $ sshpass -v -p root ssh root@192.168.0.205
	SSHPASS searching for password prompt using match "assword"
	root@192.168.0.205's password:
	SSHPASS detected prompt. Sending password.
	SSHPASS read:

	#

#. after reboot the connection won't ask to accept host key::

	# reboot
	Connection to 192.168.0.205 closed by remote host.
	Connection to 192.168.0.205 closed.
	PC $

	PC $ sshpass -v -p root ssh root@192.168.0.205
	SSHPASS searching for password prompt using match "assword"
	root@192.168.0.205's password:
	SSHPASS detected prompt. Sending password.
	SSHPASS read:

	#

Versions
--------

#. check rpi3 repo version::

	# cat /etc/version
	rpi3 @ v0.11-7-g7a400e6
	#

#. check buildroot version::

	# cat /etc/os-release
	NAME=Buildroot
	VERSION=2023.05.1
	ID=buildroot
	VERSION_ID=2023.05.1
	PRETTY_NAME="Buildroot 2023.05.1"
	#

#. check linux kernel version::

	# uname -a
	Linux rpi3 5.15.87-v8 #1 SMP PREEMPT Tue Aug 22 02:00:36 UTC 2023 aarch64 GNU/Linux
	#

Update
------

#. at host PC copy the .swu file to a http server::

	PC # cp output/rpi3_64_linux_systemd/images/rootfs.swu /var/www/html/files/

#. check rpi3 repo version::

	# cat /etc/version
	rpi3 @ v0.11-7-g7a400e6
	#

#. check current partition being used::

	# cat /proc/cmdline | sed -e 's, ,\n,g' | grep ^root=
	root=/dev/mmcblk0p2
	#

#. start the updater::

	# time /opt/update http://192.168.0.17/files/rootfs.swu
	+ URL=http://192.168.0.17/files/rootfs.swu
	+ sed 's,.* root=\([^ ]\+\).*,\1,g' /proc/cmdline
	+ CURRENT_ROOT=/dev/mmcblk0p2
	+ IMAGE_TO_WRITE=stable,copy-2
	+ wget -O /tmp/rootfs.swu http://192.168.0.17/files/rootfs.swu
	Connecting to 192.168.0.17 (192.168.0.17:80)
	saving to '/tmp/rootfs.swu'
	rootfs.swu           100% |*********************************************************| 24.5M  0:00:00 ETA
	'/tmp/rootfs.swu' saved
	+ time swupdate -v -e stable,copy-2 -i /tmp/rootfs.swu
	SWUpdate v2022.12 (Buildroot 2023.05.1)
	...
	[INFO ] : SWUPDATE successful ! SWUPDATE successful !
	[TRACE] : SWUPDATE running :  [network_initializer] : Main thread sleep again !
	[INFO ] : No SWUPDATE running :  Waiting for requests...
	[INFO ] : SWUPDATE running :  [endupdate] : SWUpdate was successful !
	real    1m 6.55s
	user    0m 3.91s
	sys     0m 5.04s
	+ echo 'Will reboot in 10 seconds'
	Will reboot in 10 seconds
	+ sleep 10
	+ reboot
	real    1m 18.87s
	user    0m 4.11s
	sys     0m 6.81s
	Connection to 192.168.0.205 closed by remote host.
	Connection to 192.168.0.205 closed.
	PC $

#. check rpi3 repo version::

	PC $ sshpass -v -p root ssh root@192.168.0.205
	...
	# cat /etc/version
	rpi3 @ v0.11-7-g7a400e6
	#

#. check current partition being used::

	# cat /proc/cmdline | sed -e 's, ,\n,g' | grep ^root=
	root=/dev/mmcblk0p3
	#

SSH host key persistemce
------------------------

#. power off the RPI3
#. wait 30 minutes (or manually reset the DHCP release for the previously used
   IP)
#. power on the RPI3
#. after boot the connection won't ask to accept host key::

	PC $ sshpass -v -p root ssh root@192.168.0.205
	SSHPASS searching for password prompt using match "assword"
	root@192.168.0.205's password:
	SSHPASS detected prompt. Sending password.
	SSHPASS read:

	#
