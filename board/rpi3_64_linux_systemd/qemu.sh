#!/bin/sh
set -e -x

BOARD="${1:-rpi3_64_linux_systemd}"
if [ ! -f "output/${BOARD}/images/sdcard.img" ]; then
    echo "You must first build the image: 'make'"
    exit 1
fi
if [ ! -f "output/${BOARD}/images/sdcard.img.qemu" ]; then
    # do not mess with the image to be burnt to the real target
    cp "output/${BOARD}/images/sdcard.img" "output/${BOARD}/images/sdcard.img.qemu"
    size=$(stat --format=%s "output/${BOARD}/images/sdcard.img.qemu")
    # NOTE always use a power of 2 as size to allow to boot the same image on target and qemu
    if [ "${size}" -le 268435456 ]; then
        qemu-img resize -f raw "output/${BOARD}/images/sdcard.img.qemu" 256M
    elif [ "${size}" -le 536870912 ]; then
        qemu-img resize -f raw "output/${BOARD}/images/sdcard.img.qemu" 512M
    elif [ "${size}" -le 1073741824 ]; then
        qemu-img resize -f raw "output/${BOARD}/images/sdcard.img.qemu" 1G
    elif [ "${size}" -le 2147483648 ]; then
        qemu-img resize -f raw "output/${BOARD}/images/sdcard.img.qemu" 2G
    else
        echo "sdcard.img is bigger than 2GiB! ${0} needs to be adapted"
        exit 1
    fi
fi
utils/docker-run qemu-system-aarch64 \
    -M raspi3b \
    -nographic \
    -kernel "output/${BOARD}/images/Image" \
    -append "root=/dev/mmcblk0p2 rootwait console=ttyAMA0" \
    -dtb "output/${BOARD}/images/bcm2710-rpi-3-b-plus.dtb" \
    -drive file="output/${BOARD}/images/sdcard.img.qemu",format=raw
