Generate or update the config fragment
======================================

#. use a clean build environment::

	make <board_defconfig>+clean
	make <board_defconfig>+menuconfig

#. select the upstream version and the upstream defconfig::

	BR2_LINUX_KERNEL=y
	BR2_LINUX_KERNEL_USE_DEFCONFIG=y
	BR2_LINUX_KERNEL_DEFCONFIG="bcmrpi3"
	BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES=""

#. build the dependencies::

	make <board_defconfig>+os-depends

#. generate the .config file equivalent to the upstream defconfig::

	utils/docker-run make -C output/<board_defconfig>/ linux-dirclean linux-configure

#. save the upstream .config::

	cp output/<board_defconfig>/build/linux-custom/.config /tmp/old

#. make any changes you wish::

	make <board_defconfig>+os-menuconfig

#. save the updated .config::

	cp output/<board_defconfig>/build/linux-custom/.config /tmp/new

#. generate the dotconfig fragment::

	buildroot/utils/diffconfig -m /tmp/old /tmp/new | tee board/common/linux/bcmrpi3_dotconfig.fragment

#. update the board defconfig::

	make <board_defconfig>+menuconfig

#. use the dotconfig fragment::

	BR2_LINUX_KERNEL_USE_DEFCONFIG=y
	BR2_LINUX_KERNEL_DEFCONFIG="bcmrpi3"
	BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES="$(BR2_EXTERNAL_RPI3_PATH)/board/common/linux/bcmrpi3_dotconfig.fragment"

#. generate the .config file equivalent to the upstream defconfig + config fragment::

	utils/docker-run make -C output/<board_defconfig>/ linux-dirclean linux-configure

#. check the .config is the expected one::

	diff -U3 output/<board_defconfig>/build/linux-custom/.config /tmp/new
