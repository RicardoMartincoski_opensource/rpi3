#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
# copied from Buildroot 2022.02.2
# (original file buildroot/board/raspberrypi/post-build.sh)
# and then updated:
# - to contain this license header
# - to fix unsafe use of variable (SC2086) detected by shellcheck

set -u
set -e

# Add a console on tty1
if [ -e "${TARGET_DIR}/etc/inittab" ]; then
    grep -qE '^tty1::' "${TARGET_DIR}/etc/inittab" || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' "${TARGET_DIR}/etc/inittab"
fi
