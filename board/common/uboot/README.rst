Generate or update the config fragment
======================================

#. use a clean build environment::

	make <board_defconfig>+clean
	make <board_defconfig>+menuconfig

#. select the upstream version and the upstream defconfig::

	BR2_TARGET_UBOOT=y
	BR2_TARGET_UBOOT_USE_DEFCONFIG=y
	BR2_TARGET_UBOOT_BOARD_DEFCONFIG="rpi_3"
	BR2_TARGET_UBOOT_CONFIG_FRAGMENT_FILES=""

#. build the dependencies::

	make <board_defconfig>+os-depends

#. generate the .config file equivalent to the upstream defconfig::

	utils/docker-run make -C output/<board_defconfig>/ uboot-dirclean uboot-configure

#. save the upstream .config::

	cp output/<board_defconfig>/build/uboot-<version>/.config /tmp/old

#. make any changes you wish::

	make <board_defconfig>+os-menuconfig

#. save the updated .config::

	cp output/<board_defconfig>/build/uboot-<version>/.config /tmp/new

#. generate the dotconfig fragment::

	buildroot/utils/diffconfig -m /tmp/old /tmp/new | tee board/common/uboot/rpi_3_dotconfig.fragment

#. update the board defconfig::

	make <board_defconfig>+menuconfig

#. use the dotconfig fragment::

	BR2_TARGET_UBOOT=y
	BR2_TARGET_UBOOT_BOARD_DEFCONFIG="rpi_3"
	BR2_TARGET_UBOOT_CONFIG_FRAGMENT_FILES="$(BR2_EXTERNAL_RPI3_PATH)/board/common/uboot/rpi_3_dotconfig.fragment"

#. generate the .config file equivalent to the upstream defconfig + config fragment::

	utils/docker-run make -C output/<board_defconfig>/ uboot-dirclean uboot-configure

#. check the .config is the expected one::

	diff -U3 output/<board_defconfig>/build/uboot-<version>/.config /tmp/new
