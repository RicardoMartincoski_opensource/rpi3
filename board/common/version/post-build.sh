#!/bin/sh
set -e -x

BASE_DIR="$(dirname "$0")/../../.."
cd "${BASE_DIR}"
echo "rpi3 @ $(git describe --long)" > "${TARGET_DIR}/etc/version"
