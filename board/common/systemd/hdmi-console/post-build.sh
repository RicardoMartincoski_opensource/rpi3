#!/bin/sh
set -u
set -e

# remove console from serial so the hdmi console becomes the main console (see
# [1]) and systemd-getty-generator ends up enabling login on hdmi
# [1] http://0pointer.de/blog/projects/serial-console.html
sed -e 's, console=tty[^0-9][^ ]*,,g' -i "${BINARIES_DIR}/rpi-firmware/cmdline.txt"
