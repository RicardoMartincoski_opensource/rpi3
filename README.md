[TOC]

# rpi3

------

## Description

Example br2-external overlay to generate images for Raspberry Pi 3.

------

## License

SPDX-License-Identifier: GPL-2.0-only

    rpi3
    Copyright (C) 2022  Ricardo Martincoski  <ricardo.martincoski@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

------

There are some files in the tree copied from other projects.
All of such files were originally licensed using a license that allows them to
be redistributed following GPL-2.0.
They have the license stated in the header of the file. For instance:

    SPDX-License-Identifier: GPL-2.0-or-later
    copied from <Project name> <version>
    (original file project_repo/path/to/file)
    and then updated:
    - to contain this license header
    - to do <something nice>

------

## How to run the tests

Assumption: using a computer with `Ubuntu 20.04.2.0`.

### Install `docker` and add user to group

```
$ sudo apt install docker.io
$ sudo groupadd docker
$ sudo usermod -aG docker $USER # NOTE: logout/login after this
$ docker run hello-world
```

### Install `git` and `make`

```
$ sudo apt install git make
```

### Download the repo

```
$ git clone --depth 1 --recurse-submodules \
  https://gitlab.com/RicardoMartincoski_opensource/rpi3.git
$ cd rpi3
```

### Generate the images

```
$ make
```

### Burn the u-boot image

```
$ sudo dd if=output/rpi3_64_uboot/images/sdcard.img of=/dev/sdX
```

#### When your have partition auto-mounter enabled on your distro

Manually unmount any previous partition and also write the whole image as a
single block (just use bs=SIZE larger than the .img file) in order to prevent
auto-mount between two blocks are written to the SD card.
```
$ mount | grep sdX # check there are mounted partitions
$ sudo umount /dev/sdX*
$ mount | grep sdX # check there is no mounted partitions
$ sudo dd if=output/rpi3_64_uboot/images/sdcard.img of=/dev/sdX bs=2G count=1
```
Wait the auto-mounter to mount the new partitions.
Manually unmount the new partitions before removing the SD card.

### Boot your u-boot image

Plug into your Raspberry Pi 3:
- the SD card;
- an HDMI display;
- a USB keyboard;

### Test the linux image using qemu

```
$ board/rpi3_64_linux_systemd/qemu.sh
```
User `root` password `root`.

**WARNING**
    This example uses ultra-INSECURE ssh configurations, do not use for real
    projects

Ctrl+A,C opens the console in which one can use `quit` to abruptly stop qemu.
```
(qemu) quit
```

### Burn the linux image

Follow buildroot/board/raspberrypi3-64/readme.txt
```
$ sudo dd if=output/rpi3_64_linux_systemd/images/sdcard.img
```
**WARNING**
    This command can take few minutes to run, depending on the size of the image
    and all speed limitations between the CPU of your computer and the SD card.
    For instance the USB revision or the SD manufacturer.

#### When your have partition auto-mounter enabled on your distro

Manually unmount any previous partition and also write the whole image as a
single block (just use bs=SIZE larger than the .img file) in order to prevent
auto-mount between two blocks are written to the SD card.
```
$ mount | grep sdX # check there are mounted partitions
$ sudo umount /dev/sdX*
$ mount | grep sdX # check there is no mounted partitions
$ sudo dd if=output/rpi3_64_linux_systemd/images/sdcard.img of=/dev/sdX bs=2G count=1
```
Wait the auto-mounter to mount the new partitions.
Manually unmount the new partitions before removing the SD card.

### Boot your linux image

Plug into your Raspberry Pi 3:
- the SD card (at least 512MiB);
- an HDMI display;
- a USB keyboard;
- an Ethernet cable;

### Connect to the linux target

At the HDMI console, using the keyboard attached to it, check the IP it got
using DHCP:
```
$ ip addr # check TARGET_IP shown at eth0
```
From your host PC, connect to the target IP:
```
$ sshpass -v -p root ssh root@TARGET_IP
```
**WARNING**
    This example uses ultra-INSECURE ssh configurations, do not use for real
    projects

### Update your linux image

At your host (the PC building the image) copy the .swu file to a webserver
(apache2 is an easy one to setup on Ubuntu machines)
```
$ cp output/rpi3_64_linux_systemd/images/rootfs.swu /HOST/PATH/TO/HTTP/SERVER/
```
At your target (either via HDMI console or vis ssh):
```
# /opt/update http://HOST_IP_HTTP_SERVER/rootfs.swu
```

------

## Caches used

This br2-external enables 2 caches for both local build and build at GitLab CI:
- download cache: automatically done by buildroot, but the directory that
  contains the download cache is configured to be inside the br2-external in
  order to improve cache usage on GitLab CI;
- ccache: also handled by buildroot, but the use of ccache is enabled in the
  defconfig, the path to the ccache points to inside the br2-external and each
  defconfig has its own ccache subfolder in order to improve cache usage.

### Comparison with/without cache

In order to measure this in the GitLab CI, 2 pipelines were started for the same
sha1:
1. Clear the caches using `Clear runner caches` at GitLab CI Pipelines page
2. Start a
   [pipeline to measure build time with caches at 0% hitrate](https://gitlab.com/RicardoMartincoski_opensource/rpi3/-/pipelines/976366067)
3. Wait pipeline to complete, and record the build times for each job
4. Start a
   [pipeline to measure build time with caches at 100% hitrate](https://gitlab.com/RicardoMartincoski_opensource/rpi3/-/pipelines/976311675)
5. Wait pipeline to complete, and record the build times for each job

| Job name                                                 | Build time (hit rate 0%) | Build time (hit rate 100%) |
|:---------------------------------------------------------|-------------------------:|---------------------------:|
| pipeline                                                 |                    94:04 |                      57:27 |
| check-package                                            |                     0:50 |                       0:39 |
| generate-gitlab-ci-yml                                   |                     0:50 |                       0:41 |
| boards-pipeline                                          |                    93:17 |                      56:45 |
| rpi3-64-uboot-config                                     |                     0:43 |                       0:53 |
| rpi3-64-uboot-source                                     |                     3:03 |                       1:01 |
| rpi3-64-uboot-toolchain                                  |                     5:40 |                       5:48 |
| rpi3-64-uboot-os-depends                                 |                     3:55 |                       3:11 |
| rpi3-64-uboot-os                                         |                     3:07 |                       2:32 |
| rpi3-64-uboot-init                                       |                     2:14 |                       2:07 |
| rpi3-64-uboot-updater                                    |                     1:34 |                       1:14 |
| rpi3-64-uboot-all                                        |                     2:56 |                       2:19 |
| rpi3-64-uboot-image-with-target-packages-rebuilt         |                     2:51 |                       2:36 |
| rpi3-64-uboot-image-regenerated                          |                     2:36 |                       2:25 |
| rpi3-64-uboot-legal-info                                 |                     1:57 |                       1:58 |
| rpi3-64-linux-sysv-config                                |                     0:41 |                       1:03 |
| rpi3-64-linux-sysv-source                                |                     3:35 |                       1:10 |
| rpi3-64-linux-sysv-toolchain                             |                     6:14 |                       6:34 |
| rpi3-64-linux-sysv-os-depends                            |                     6:07 |                       4:07 |
| rpi3-64-linux-sysv-os                                    |                    28:04 |                       8:35 |
| rpi3-64-linux-sysv-init                                  |                     3:22 |                       3:36 |
| rpi3-64-linux-sysv-updater                               |                     7:38 |                       4:15 |
| rpi3-64-linux-sysv-all                                   |                    12:40 |                       8:09 |
| rpi3-64-linux-sysv-image-with-target-packages-rebuilt    |                     9:16 |                       9:58 |
| rpi3-64-linux-sysv-image-regenerated                     |                    10:51 |                       9:07 |
| rpi3-64-linux-sysv-legal-info                            |                     2:37 |                       2:53 |
| rpi3-64-linux-systemd-config                             |                     0:43 |                       1:06 |
| rpi3-64-linux-systemd-source                             |                     3:30 |                       1:10 |
| rpi3-64-linux-systemd-toolchain                          |                     6:12 |                       6:30 |
| rpi3-64-linux-systemd-os-depends                         |                     6:45 |                       4:06 |
| rpi3-64-linux-systemd-os                                 |                    25:36 |                       9:12 |
| rpi3-64-linux-systemd-init                               |                    16:07 |                       8:14 |
| rpi3-64-linux-systemd-updater                            |                     5:27 |                       4:06 |
| rpi3-64-linux-systemd-all                                |                    15:31 |                       9:17 |
| rpi3-64-linux-systemd-image-with-target-packages-rebuilt |                    13:00 |                      12:57 |
| rpi3-64-linux-systemd-image-regenerated                  |                    11:02 |                      10:55 |
| rpi3-64-linux-systemd-legal-info                         |                     2:47 |                       2:54 |
