#!/usr/bin/env bash
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (C) 2023  Ricardo Martincoski  <ricardo.martincoski@gmail.com>
set -o errexit -o pipefail

if [ -z "${1}" ]; then
    echo "usage: ${0} buildroot_dir sha1_before sha1_after defconfig [br2_external_dir]"
    echo "    buildroot_dir - path to a buildroot git clone"
    echo "    sha1_before - sha1 which buildroot is being upgraded from"
    echo "    sha1_after - sha1 which buildroot is being upgraded to"
    echo "    defconfig - name of the defconfig, without the sufix _defconfig"
    echo "    br2_external_dir - path to a br2-external"
    exit 0
fi

buildroot_dir=$(readlink -f "${1}")
sha1_before="${2}"
sha1_after="${3}"
defconfig="${4}"
br2_external_dir="${5}"

if [ ! -d "${buildroot_dir}" ]; then
    echo "ERROR: ${buildroot_dir} is not a directory"
    exit 1
fi
if ! grep -q Buildroot "${buildroot_dir}/README" 2>/dev/null; then
    echo "ERROR: ${buildroot_dir} is not a buildroot copy"
    exit 1
fi
if [ ! -e "${buildroot_dir}/.git" ]; then
    echo "ERROR: ${buildroot_dir} is not a buildroot git tree"
    exit 1
fi
if [ -z "${sha1_before}" ]; then
    echo "ERROR: you must pass the buildroot sha1 to update from"
    exit 1
fi
if [ -z "${sha1_after}" ]; then
    echo "ERROR: you must pass the buildroot sha1 to update to"
    exit 1
fi
if ! git -C "${buildroot_dir}" rev-parse --verify --quiet "${sha1_before}" >/dev/null; then
    echo "ERROR: ${buildroot_dir} has no sha1 or reference ${sha1_before}"
    exit 1
fi
if ! git -C "${buildroot_dir}" rev-parse --verify --quiet "${sha1_after}" >/dev/null; then
    echo "ERROR: ${buildroot_dir} has no sha1 or reference ${sha1_after}"
    exit 1
fi
if [ -z "${defconfig}" ]; then
    echo "ERROR: you must pass a defconfig to update"
    exit 1
else
   defconfig="${defconfig}_defconfig"
fi
if [ -n "${br2_external_dir}" ]; then
    if [ ! -d "${br2_external_dir}" ]; then
        echo "ERROR: ${br2_external_dir} is not a directory"
        exit 1
    fi
    br2_external_dir=$(readlink -f "${br2_external_dir}")
fi

if [ -n "${br2_external_dir}" ]; then
    temp_dir="${br2_external_dir}/.temp"
else
    temp_dir="${buildroot_dir}/.temp"
fi
buildroot_dir_before="${temp_dir}/buildroot-${sha1_before}"
buildroot_dir_after="${temp_dir}/buildroot-${sha1_after}"
build_output_dir_before="${temp_dir}/build_output-${sha1_before}"
build_output_dir_after="${temp_dir}/build_output-${sha1_after}"

mkdir -p "${temp_dir}"
rm -rf "${buildroot_dir_before}"
rm -rf "${buildroot_dir_after}"
rm -rf "${build_output_dir_before}"
rm -rf "${build_output_dir_after}"
git -C "${buildroot_dir}" worktree prune
git -C "${buildroot_dir}" worktree add "${buildroot_dir_before}" "${sha1_before}"
git -C "${buildroot_dir}" worktree add "${buildroot_dir_after}" "${sha1_after}"
if [ -n "${br2_external_dir}" ]; then
    make -C "${buildroot_dir_before}" BR2_EXTERNAL="${br2_external_dir}" O="${build_output_dir_before}" "${defconfig}"
    make -C "${buildroot_dir_after}" BR2_EXTERNAL="${br2_external_dir}" O="${build_output_dir_after}" "${defconfig}"
else
    make -C "${buildroot_dir_before}" O="${build_output_dir_before}" "${defconfig}"
    make -C "${buildroot_dir_after}" O="${build_output_dir_after}" "${defconfig}"
fi
cp -f "${build_output_dir_before}/.config" "${build_output_dir_after}/.config"
make -C "${build_output_dir_after}" menuconfig
make -C "${build_output_dir_after}" savedefconfig
