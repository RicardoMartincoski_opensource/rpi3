################################################################################
#
# Build the swu image
#
################################################################################

ROOTFS_SWU_SW_DESCRIPTION = $(call qstrip,$(BR2_TARGET_ROOTFS_SWU_CONFIG))
ROOTFS_SWU_SCRIPTS = $(call qstrip,$(BR2_TARGET_ROOTFS_SWU_SCRIPTS))
ROOTFS_SWU_FILES = sw-description $(call qstrip,$(BR2_TARGET_ROOTFS_SWU_FILES))

ROOTFS_SWU_DEPENDENCIES = rootfs-genimage

define ROOTFS_SWU_CMD
	(cd $(BINARIES_DIR) ; \
		$(RM) sw-description; \
		cp -f $(ROOTFS_SWU_SW_DESCRIPTION) sw-description; \
		for i in $(ROOTFS_SWU_SCRIPTS); do \
			cp --force $${i} .; done ; \
		for i in $(ROOTFS_SWU_FILES); do \
			if [ -f $${i%.gz} ]; then \
				gzip --force --keep $${i%.gz}; fi; done ; \
		for i in $(ROOTFS_SWU_FILES); do echo $$i; done | \
			cpio --create --verbose --format=crc > $@)
endef

$(eval $(rootfs))
