################################################################################
#
# Build the root filesystem image using genimage
#
################################################################################

ROOTFS_GENIMAGE_CONFIG = $(call qstrip,$(BR2_TARGET_ROOTFS_GENIMAGE_CONFIG))

ROOTFS_GENIMAGE_DEPENDENCIES = host-genimage

ifeq ($(BR2_TARGET_ROOTFS_EXT2),y)
ROOTFS_GENIMAGE_DEPENDENCIES += rootfs-ext2
endif

ROOTFS_GENIMAGE_TMP_DIR = $(FS_DIR)/rootfs.genimage.tmp
define ROOTFS_GENIMAGE_CREATE_TEMPDIR
	$(RM) -rf $(ROOTFS_GENIMAGE_TMP_DIR)
	mkdir -p $(ROOTFS_GENIMAGE_TMP_DIR)
endef
ROOTFS_GENIMAGE_PRE_CMD_HOOKS += ROOTFS_GENIMAGE_CREATE_TEMPDIR

define ROOTFS_GENIMAGE_CMD
	$(HOST_DIR)/bin/genimage \
		--rootpath "$$(mktemp -d)" \
		--tmppath "$(ROOTFS_GENIMAGE_TMP_DIR)" \
		--inputpath "$(BINARIES_DIR)" \
		--outputpath "$(BINARIES_DIR)" \
		--config "$(ROOTFS_GENIMAGE_CONFIG)"
endef

$(eval $(rootfs))
