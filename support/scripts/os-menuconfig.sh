#!/usr/bin/env bash
set -e

target="$1"
builddir="$2"

dotconfig="${builddir}/.config"
dotconfig_backup="${builddir}/.config-os-menuconfig"
linux_dotconfig="${builddir}/build/linux-custom/.config"
linux_dotconfig_old="${builddir}/.linux-config-os-menuconfig-base"
linux_dotconfig_merge="${builddir}/.linux-config-os-menuconfig-merge"
linux_dotconfig_new="${builddir}/.linux-config-os-menuconfig-menuconfig"

if grep -q 'BR2_TARGET_UBOOT_BUILD_SYSTEM_KCONFIG=y' "${dotconfig}"; then
	echo "--- (uboot) ${target} ---"
	make -C "${builddir}" uboot-menuconfig
	make -C "${builddir}" uboot-savedefconfig
	make -C "${builddir}" uboot-update-defconfig
else
	echo "--- (skip uboot) ${target} ---"
fi

if grep -q 'BR2_LINUX_KERNEL=y' "${dotconfig}"; then
	echo "--- (linux) ${target} ---"
	if grep -q 'BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES=""' "${dotconfig}"; then
		make -C "${builddir}" linux-menuconfig
		make -C "${builddir}" linux-savedefconfig
		make -C "${builddir}" linux-update-defconfig
	else
		# shellcheck disable=SC2016
		fragment_file="$(grep 'BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES=' "${dotconfig}" | sed -e 's,^BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES=",,g' -e 's,"$,,g' -e 's,\$(BR2_EXTERNAL_RPI3_PATH)/,,g')"
		cp -f "${dotconfig}" "${dotconfig_backup}"
		echo 'BR2_LINUX_KERNEL_CONFIG_FRAGMENT_FILES=""' >> "${dotconfig}"
		make -C "${builddir}" olddefconfig
		make -C "${builddir}" linux-dirclean linux-configure
		cp -f "${linux_dotconfig}" "${linux_dotconfig_old}"
		cp -f "${dotconfig_backup}" "${dotconfig}"
		make -C "${builddir}" linux-dirclean linux-configure
		cp -f "${linux_dotconfig}" "${linux_dotconfig_merge}"
		make -C "${builddir}" linux-menuconfig
		cp -f "${linux_dotconfig}" "${linux_dotconfig_new}"
		buildroot/utils/diffconfig -m "${linux_dotconfig_old}" "${linux_dotconfig_new}" > "${fragment_file}"
	fi
else
	echo "--- (skip linux) ${target} ---"
fi
