
.${board}_cache: &${board}_cache
  key: ${board}
  paths:
    - .ccache/
    - download/
  policy: pull-push

${board}_config:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  script:
    - make ${board}+config

${board}_source:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_config
  script:
    - make ${board}+source

${board}_toolchain:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_source
  script:
    - make ${board}+toolchain

${board}_os-depends:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_toolchain
  script:
    - make ${board}+os-depends

${board}_os:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_os-depends
  script:
    - make ${board}+os

${board}_init:
  extends: .intermediary_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_os
  script:
    - make ${board}+init

${board}_updater:
  extends: .intermediary_build_no_artifacts
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_init
  script:
    - make ${board}+updater

# "all" and "updater" jobs lead to "FATAL: too large" when uploading artifacts containing output/
.${board}_last_job_saving_pipeline_artifacts:
  needs:
    job: ${board}_init

${board}_all:
  extends: .final_build
  cache:
    <<: *${board}_cache
  needs:
    - job: ${board}_updater
      artifacts: false
    - !reference [.${board}_last_job_saving_pipeline_artifacts, needs]
  script:
    - make ${board}+all

.${board}_rebuild:
  extends: .final_build
  cache:
    <<: *${board}_cache
    policy: pull
  needs:
    - job: ${board}_all
      artifacts: false
    - !reference [.${board}_last_job_saving_pipeline_artifacts, needs]
  before_script:
    - make ${board}+all
    - rm -f output/*/br.log
    - rm -f output/*/build/build-time.log
    - rm -f output/*/build/packages-file-list.txt

${board}_image-regenerated:
  extends: .${board}_rebuild
  cache:
    <<: *${board}_cache
    policy: pull
  script:
    - make ${board}+image

${board}_image-with-target-packages-rebuilt:
  extends: .${board}_rebuild
  cache:
    <<: *${board}_cache
    policy: pull
  script:
    - make ${board}+rebuild

${board}_legal-info:
  cache:
    <<: *${board}_cache
    policy: pull
  needs:
    - job: ${board}_toolchain
  before_script:
    - rm -f output/*/br.log
  script:
    - make ${board}+legal-info
  artifacts:
    when: always
    paths:
      - output/*/br.log
      - output/*/legal-info/
